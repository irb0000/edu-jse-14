package com.nlmk.sychikov.tm.repository;

import com.nlmk.sychikov.tm.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Projects storage
 */
public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();
    private final Map<String, List<Project>> nameIndex = new HashMap<>();

    private void addToRepo(final Project project) {
        addToNameIndex(project);
        projects.add(project);
    }

    private List<Project> findInNameIndex(Project project) {
        return nameIndex.get(project.getName());
    }

    private void addToNameIndex(Project project) {
        nameIndex.computeIfAbsent(project.getName(), k -> new ArrayList<Project>()).add(project);
    }

    private Project removeFromNameIndex(Project project) {
        final List<Project> projectList = findInNameIndex(project);
        if (projectList != null) projectList.remove(project);
        if (projectList.isEmpty()) nameIndex.remove(project.getName());
        return project;
    }

    public Project removeProject(Project project) {
        removeFromNameIndex(project);
        if (projects.remove(project)) return project;
        return null;
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        addToRepo(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description);
        assignToUser(project, userId);
        addToRepo(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        addToRepo(project);
        return project;
    }

    public int getRepositorySize() {
        return projects.size();
    }

    public Project assignToUser(final Project project, final Long userId) {
        project.setUserId(userId);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        removeFromNameIndex(project);
        project.setName(name);
        project.setDescription(description);
        addToNameIndex(project);
        return project;
    }

    public void clear() {
        nameIndex.clear();
        projects.clear();
    }

    public void clear(final Long userId) {
        final List<Project> userProjects = findAllByUserId(userId);
        if (userProjects == null) return;
        for (final Project project : userProjects) {
            removeProject(project);
        }
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    public Project findById(final Long id, final Long userId) {
        for (final Project project : projects) {
            if (id.equals(project.getId()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    public List<Project> findByName(final String name) {
        return nameIndex.get(name);
    }

    public List<Project> findByName(final String name, final Long userId) {
        final List<Project> projectList= new ArrayList<>();
        for (final Project project : nameIndex.get(name)) {
            if (userId.equals(project.getUserId())) projectList.add(project);
        }
        return projectList;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        removeProject(project);
        return project;
    }

    public Project removeById(final Long id, final Long userId) {
        final Project project = findById(id, userId);
        if (project == null) return null;
        removeProject(project);
        return project;
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        removeProject(project);
        return project;
    }

//    public Project removeByName(final String name, final Long userId) {
//        final Project project = findByName(name, userId);
//        if (project == null) return null;
//        projects.remove(project);
//        return project;
//    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAllByUserId(final Long userId) {
        List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(project);
        }
        return result;
    }

}

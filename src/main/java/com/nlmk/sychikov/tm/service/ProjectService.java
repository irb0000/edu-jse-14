package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (description == null || description.isEmpty()) return create(name);
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (userId == null) return create(name, description);
        if (description == null || description.isEmpty()) return create(name);
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(final Long id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (id == null) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project findByIndex(final int index) {
        if (index > projectRepository.getRepositorySize() - 1 || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project assignToUser(Long projectId, Long userId) {
        if (projectId == null) return null;
        if (userId == null) return null;
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        return projectRepository.assignToUser(project, userId);
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    public Project findById(final Long id, final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return projectRepository.findById(id, userId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

    /*Following methods aren't used now. See ProjectTaskService for reference*/
    public List<Project> findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(final int index) {
        if (index > projectRepository.getRepositorySize() - 1 || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

}

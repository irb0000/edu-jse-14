package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (userId == null) return create(name, description);
        if (description == null || description.isEmpty()) return create(name);
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (id == null) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public void clear(final Long userId) {
        if (userId == null) return;
        taskRepository.clear(userId);
    }

    public Task findByIndex(final int index) {
        if (index > taskRepository.getRepositorySize() - 1 || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    public Task assignToUser(Task task, Long userId) {
        if (task == null) return null;
        if (userId == null) return null;
        return taskRepository.assignToUser(task, userId);
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task findById(final Long id, final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.findById(id, userId);
    }

    public List<Task> findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeById(final Long id, final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(final int index) {
        if (index > taskRepository.getRepositorySize() - 1 || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    public Task remove(final Task task) {
        if (task == null) return null;
        return taskRepository.removeTask(task);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public int getRepositorySize() {
        return taskRepository.getRepositorySize();
    }

    public List<Task> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

}
